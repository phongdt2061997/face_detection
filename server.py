from flask import Flask
from flask import request
import download_images
import demo
import os, shutil

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'OK'

@app.route('/find', methods=['GET', 'POST'])
def find_user():
    userhome = os.path.expanduser('~')
    params = request.json
    fbid = params["fbid"]
    ids = params["ids"]
    images = params["images"]
    
    index = 1
    if (len(images) > 0):
        print("Download checkpoint images ...")
        user_image_folder_path = str(userhome) + "/Desktop/user_image"
        temp_images_folder_path = str(userhome) + "/Desktop/temp"
        fbid_folder_path = os.path.join(temp_images_folder_path, fbid)
        fbid_cp_img_folder_path = os.path.join(user_image_folder_path, fbid)

        if (not os.path.exists(user_image_folder_path)):
            os.mkdir(user_image_folder_path)

        if (not os.path.exists(temp_images_folder_path)):
            os.mkdir(temp_images_folder_path)

        if (not os.path.exists(fbid_folder_path)):
            os.mkdir(fbid_folder_path)
        
        if (not os.path.exists(fbid_cp_img_folder_path)):
            os.mkdir(fbid_cp_img_folder_path)

        for image_url in images:
            filename = os.path.join(fbid_cp_img_folder_path, "img" + str(index) + ".jpg")
            res = "OK"
            if ("https://" in image_url):
                res = download_images.download_image_from_url(image_url, filename)
            else:
                download_images.convert_base64_to_image(image_url, filename)

            print("Downloaded img" + str(index) + ": " + res)
            index += 1
    else:
        return 'Images empty !!!'

    download_response = ""
    if (ids == ""):
        if ("data" in params):
            data = params["data"]
            if (len(data) > 0):
                download_response = download_images.storage_images_from_base64(fbid, data)
            else:
                download_response = "Data empty !!"
    else:
        print("Download images of ids ...")
        download_response = download_images.download_images_from_ids(ids)
            
    if (download_response == "OK"):
        return demo.dectect_face(fbid)
    else:
        return 'Download Images Errors: ' + download_response
    
    return str(request.json)
