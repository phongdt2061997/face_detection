import face_recognition
import cv2, os, multiprocessing, queue, re, shutil

# files_queue = queue.Queue()
userhome = os.path.expanduser('~')
result_list = []
known_face_encodings = []

def cropFaceFromImage(img_path):
    image = cv2.imread(img_path)

    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    thresh = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV,51,9)

    # Fill rectangular contours
    cnts = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    for c in cnts:
        cv2.drawContours(thresh, [c], -1, (255,255,255), -1)


    # Morph open
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (9,9))
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=4)

    # Draw rectangles
    cnts = cv2.findContours(opening, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]

    for c in cnts:
        approx = cv2.approxPolyDP(c, 0.01* cv2.arcLength(c, True), True)
        x,y,w,h = cv2.boundingRect(c)
        
        if (len(approx) == 4) and (abs(w-h) <= 10):
            img_h, img_w = image.shape[:2]
            padding = 30
            left = x - padding
            top = y - padding
            height = top + h + (padding * 2)
            width = left + w + (padding * 2)

            left = 0 if left < 0 else left
            top = 0 if top < 0 else top
            height = img_h if height > img_h else height
            width = img_w if width > img_w else width

            # cv2.rectangle(image, (left, top), (width, height), (36,255,12), 2)

            # cv2.imshow(img_path, image)

            return image[top:height, left:width]

    # cv2.imshow('thresh', thresh)
    # cv2.imshow('opening', opening)
    # cv2.imshow('image', image)

    # cv2.waitKey()

def files(path):
    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)):
            yield file

def log_result(result):
    result_list.append(result)

def log_result_process_cp_img(result):
    known_face_encodings.extend(result)

def compare_faces(image_path, known_face_encodings):
    img = face_recognition.load_image_file(image_path)
    unknown_face_locations = face_recognition.face_locations(img, number_of_times_to_upsample = 1, model = 'hog')
    unknown_face_encodings = face_recognition.face_encodings(img, unknown_face_locations)
    os.remove(image_path)
    for unknown_face_encoding in unknown_face_encodings:
        results = face_recognition.compare_faces(known_face_encodings, unknown_face_encoding)
        print(image_path, len(unknown_face_locations), results)
        if (len(results) > 0):
            if (results[0] == True):
                # cv2.imshow(file, img)
                return 'True_' + image_path

    return 'False_' + image_path

def process_cp_img(img_path):
    try:
        if (os.path.exists(img_path)):
            img_face = cropFaceFromImage(img_path)
            face_locations = face_recognition.face_locations(img_face, number_of_times_to_upsample = 1, model = 'cnn')
            print(img_path, face_locations)
            if (len(face_locations) > 0):
                os.remove(img_path)
                return face_recognition.face_encodings(img_face, face_locations)
            # cv2.imshow(img_path, img_face)

    except Exception as e:
        print(e)
        pass
    return []

def dectect_face(fbid):
    global result_list
    global known_face_encodings
    result_list = []
    imgs_folder = os.path.join(str(userhome) + '/Desktop/temp', fbid)
    imgs_path = [str(userhome) + '/Desktop/user_image/'+ fbid +'/img1.jpg', str(userhome) + '/Desktop/user_image/'+ fbid +'/img2.jpg', str(userhome) + '/Desktop/user_image/'+ fbid +'/img3.jpg']

    known_face_encodings = []
    pool_process_cp_img = multiprocessing.Pool(processes=3)
    for img_path in imgs_path:
        pool_process_cp_img.apply_async(process_cp_img, args= (img_path, ), callback=log_result_process_cp_img)
    pool_process_cp_img.close()
    pool_process_cp_img.join()
    
    if (len(known_face_encodings) <= 0):
        return "User Images empty !!!"

    pool = multiprocessing.Pool(processes=6)

    for file_path in files(imgs_folder):
        pool.apply_async(compare_faces, args=(os.path.join(str(userhome) + '/Desktop/temp', fbid, file_path), known_face_encodings, ), callback = log_result)
    pool.close()
    pool.join()

    result_obj = {}
    for file_path in result_list:
        s = file_path.split('_')
        result = s[0]
        uid = s[1].replace(str(userhome) + '/Desktop/temp/' + fbid + '/', '')
        user_name = s[2]

        if (uid not in result_obj):
            result_obj[uid] = {'name': user_name,'total': 0, 'detected': 0}
        
        if (result == 'True'):
            result_obj[uid]['detected'] += 1

        result_obj[uid]['total'] += 1
        result_obj[uid]['same_persentage'] = round((result_obj[uid]['detected'] / result_obj[uid]['total']) * 100, 2)
    

    if (os.path.exists(str(userhome) + '/Desktop/temp/' + fbid)):
        shutil.rmtree(str(userhome) + '/Desktop/temp/' + fbid)
    
    if (os.path.exists(str(userhome) + '/Desktop/user_image/' + fbid)):
        shutil.rmtree(str(userhome) + '/Desktop/user_image/' + fbid)

    print (result_obj)
    return result_obj

# print(img_face)
# face_locations = face_recognition.face_locations(img_face, number_of_times_to_upsample = 1, model = 'cnn')
# print(face_locations)

# if (len(face_locations) > 0):
#     top, right, bottom, left = face_locations[0]
#     cv2.rectangle(img_face, (left, top), (right, bottom), (0, 0, 255), 2)

# cv2.imshow('face', img)
# cv2.waitKey()

# dectect_face()
