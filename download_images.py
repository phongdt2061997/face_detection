import requests, shutil, os, queue, threading, base64

userhome = os.path.expanduser('~')

def thread_download_photos(q):
    while (True):
        photo = q.get()
        print("Download " + photo["id"] + " ...")
        response = requests.get(photo["source"])
        fw = open(str(userhome) + "/Desktop/temp/" + photo["uid"] + "_" + photo["name"] + "_" + photo["id"] + ".jpg", "wb")
        fw.write(response.content)
        fw.close()
        q.task_done()
            

def download_images_from_ids(str_ids, limit = 6):
    # if (os.path.exists('temp')):
    #     shutil.rmtree('temp')
    # os.mkdir('temp')
    # str_ids = '100026178602683,100036023842768,100014950729212,100022746736358,100010868672497,100028260076856,100022040920023,1420623625,100024250151457,100010826223241,100009111230663,100036148423826,100033386891980,100028638550582,100025004553463,100002030750007,100033609420461,100028454857110,100015003555399,100021741791536,100028405598185'
    access_token = ''
    request_link = 'https://graph.facebook.com/?ids='+ str_ids +'&fields=name,albums.limit(5){type,name,photos.limit(5){source}}&access_token=' + access_token
    r = requests.get(request_link)
    try:
        photos_queue = queue.Queue()

        for i in range(5):
            t = threading.Thread(target=thread_download_photos, args=(photos_queue, ))
            t.daemon = False
            t.start()

        for uid, data in r.json().items():
            print("Download photos from " + uid)
            if ("albums" in data):
                albums = data["albums"]["data"]
                photos = get_images_from_albums(albums, 'profile')
                if (len(photos) < limit):
                    photos.extend(get_images_from_albums(albums, 'wall'))

                if (len(photos) < limit):
                    photos.extend(get_images_from_albums(albums, 'mobile'))

                for photo in photos:
                    photo["uid"] = uid
                    photo["name"] = data["name"]
                    photos_queue.put(photo)
            
        photos_queue.join()
                    
        return 'OK'
    except Exception as e:
        return str(e)

def get_images_from_albums(albums, album_type = 'profile'):
    results = []
    for album in albums:
        if ("photos" in album):
            if (str(album["type"]) == album_type):
                for photo in album["photos"]["data"]:
                    results.append(photo)
    return results

def download_image_from_url(img_url, save_as = 'image1.jpg', width = 500):
    try:
        s = img_url.split('&')
        url_part = []
        for item in s:
            if ("w=" in item):
                url_part.append("w=500")
            else:
                url_part.append(item)

        img_url = "&".join(url_part)
        print(img_url)
        response = requests.get(img_url)
        fw = open(save_as, "wb")
        fw.write(response.content)
        fw.close()
        return "OK"
    except Exception as e:
        return str(e)

def convert_base64_to_image(img_str, filename):
    img_data = base64.b64decode(img_str)
    with open(filename, 'wb') as f:
        f.write(img_data)

def thread_convert_base64_to_img(q):
    while (True):
        photo = q.get()
        print("Download " + photo["filename"] + " ...")
        convert_base64_to_image(photo["source"], photo["filename"])
        q.task_done()

def storage_images_from_base64(fbid, data):
    # if (os.path.exists('temp')):
    #     shutil.rmtree('temp')
    # os.mkdir('temp')

    photos_queue = queue.Queue()

    for i in range(5):
        t = threading.Thread(target=thread_convert_base64_to_img, args=(photos_queue, ))
        t.daemon = False
        t.start()

    for item in data:
        uid = item["uid"]
        name = item["name"]
        photos = item["photos"]
        for photo in photos:
            filename = os.path.join(str(userhome) + "/Desktop/temp", fbid, uid + "_" + name + "_" + str(photo["id"]) + ".jpg")
            photos_queue.put({"filename": filename, "source": photo["source"]})

    photos_queue.join()
    
    return "OK"
